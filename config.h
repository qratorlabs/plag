#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>

#define SIZE (10 * 1<<20)
#define BUFLEN 1024
typedef uint32_t pos_t;
typedef int mask_t;

#define STRICT_MAX_DEPTH 32 // less than 32 is aligned anyway

#endif /* !CONFIG_H */

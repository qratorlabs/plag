#ifndef LIB_H
#define LIB_H

#include "config.h"

#define IP4LEN 32
#define IP6LEN 128
#define IPMAXLEN IP6LEN
#define IPMAXB (IPMAXLEN/8)

struct in4_addr { unsigned char s4_addr[4]; };
typedef int bit_t;
typedef bit_t *bits;

void bits2ip4(bits b, mask_t m, char *s, int size);
void bits2ip6(bits b, mask_t m, char *s, int size);
void ip42bits(char *s, bits b, mask_t *m);
void ip62bits(char *s, bits b, mask_t *m);

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

extern const char *program_invocation_name;

#define init_err() { program_invocation_name = argv[0]; }

void error(int status, int errnum, const char *fmt, ...);

#endif /* !LIB_H */

CC=gcc
CFLAGS= -O3

all: plagmax plageq

plagmax: plagmax.c lib.o lib.h config.h
	$(CC) $(CFLAGS) -o $@ plagmax.c lib.o

plageq: plageq.c lib.o lib.h config.h
	$(CC) $(CFLAGS) -o $@ plageq.c lib.o

lib.o: lib.c lib.h
	$(CC) $(CFLAGS) -o $@ -c lib.c

clean:
	rm plagmax plageq lib.o
